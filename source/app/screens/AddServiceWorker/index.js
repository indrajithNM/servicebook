import React, {useState} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Picker,
} from 'react-native';
import {BaseStyle, useTheme} from '@config';
import {useTranslation} from 'react-i18next';
import {Header, SafeAreaView, Icon, Text, Button, TextInput} from '@components';
import styles from './styles';

export default function AddServiceWorker({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();

  const [service, setService] = useState('');
  const [details, setDetails] = useState('');

  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [address, setAddress] = useState('');
  const [success, setSuccess] = useState({
    service: true,
    details: true,
    address: true,
  });
  const [loading, setLoading] = useState(false);

  /**
   * @description Called when user sumitted form
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   */
  const onSubmit = () => {
    if (service === '' || details === '' || address === '') {
      setSuccess({
        ...success,
        service: service !== '' ? true : false,
        details: details !== '' ? true : false,
        address: address !== '' ? true : false,
      });
    } else {
      setLoading(true);
      setTimeout(() => {
        setLoading(true);
        navigation.goBack();
      }, 500);
    }
  };

  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  return (
    <SafeAreaView style={BaseStyle.safeAreaView} forceInset={{top: 'always'}}>
      <Header
        title={t('Add A Service Worker')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}
        keyboardVerticalOffset={offsetKeyboard}
        style={{flex: 1}}>
        <ScrollView contentContainerStyle={{paddingHorizontal: 20}}>
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setName(text)}
            placeholder={t('Name')}
            keyboardType="default"
            success={success.name}
            value={name}
          />
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setAddress(text)}
            placeholder={t('Email Address')}
            keyboardType="email-address"
            success={success.address}
            value={address}
          />
          <Text headline style={{marginVertical: 10}}>
            {t('Or')}
          </Text>
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setName(text)}
            placeholder={t('Name')}
            keyboardType="default"
            success={success.name}
            value={name}
          />
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setPhone(text)}
            placeholder={t('Phone Number')}
            keyboardType="phone-pad"
            success={success.phone}
            value={phone}
          />
        </ScrollView>
        <View style={{paddingVertical: 15, paddingHorizontal: 20}}>
          <Button
            loading={loading}
            full
            onPress={() => {
              onSubmit();
            }}>
            {t('Submit')}
          </Button>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
