import React, {useState} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Picker,
} from 'react-native';
import {BaseStyle, useTheme} from '@config';
import {useTranslation} from 'react-i18next';
import {Header, SafeAreaView, Icon, Text, Button, TextInput} from '@components';
import styles from './styles';

export default function MakeRequest({navigation}) {
  const {colors} = useTheme();
  const {t} = useTranslation();

  const [service, setService] = useState('');
  const [details, setDetails] = useState('');
  const [address, setAddress] = useState('');
  const [success, setSuccess] = useState({
    service: true,
    details: true,
    address: true,
  });
  const [loading, setLoading] = useState(false);

  /**
   * @description Called when user sumitted form
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   */
  const onSubmit = () => {
    if (service === '' || details === '' || address === '') {
      setSuccess({
        ...success,
        service: service !== '' ? true : false,
        details: details !== '' ? true : false,
        address: address !== '' ? true : false,
      });
    } else {
      setLoading(true);
      setTimeout(() => {
        setLoading(true);
        navigation.goBack();
      }, 500);
    }
  };

  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  return (
    <SafeAreaView style={BaseStyle.safeAreaView} forceInset={{top: 'always'}}>
      <Header
        title={t('make_a_request')}
        renderLeft={() => {
          return (
            <Icon
              name="arrow-left"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressLeft={() => {
          navigation.goBack();
        }}
      />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}
        keyboardVerticalOffset={offsetKeyboard}
        style={{flex: 1}}>
        <ScrollView contentContainerStyle={{paddingHorizontal: 20}}>
          <Picker
            style={styles.picker}
            selectedValue={service}
            success={success.service}
            onValueChange={(itemValue, itemIndex) => setService(itemValue)}>
            <Picker.Item label="Select a Service Category" value="" />
            <Picker.Item label="Travel" value="travel" />
            <Picker.Item label="Plumber" value="plumber" />
            <Picker.Item label="Handyman" value="handyman" />
            <Picker.Item label="Agriculture" value="agriculture" />
            <Picker.Item label="Insurance" value="insurance" />
            <Picker.Item label="Auto" value="auto" />
            <Picker.Item label="Mechanic" value="mechanic" />
          </Picker>
          <TextInput
            style={{marginTop: 10, height: 120}}
            onChangeText={text => setDetails(text)}
            textAlignVertical="top"
            multiline={true}
            placeholder={t('Details')}
            success={success.details}
            value={details}
          />
          <TextInput
            style={{marginTop: 10}}
            onChangeText={text => setAddress(text)}
            placeholder={t('Address')}
            keyboardType="email-address"
            success={success.address}
            value={address}
          />
        </ScrollView>
        <View style={{paddingVertical: 15, paddingHorizontal: 20}}>
          <Button
            loading={loading}
            full
            onPress={() => {
              onSubmit();
            }}>
            {t('Submit')}
          </Button>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
